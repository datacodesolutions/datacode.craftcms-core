<?php
/**
 * Datacode plugin for Craft CMS 3.x
 *
 * Datacode Solutions Plugin
 *
 * @link      datacodesolutions.com
 * @copyright Copyright (c) 2017 Datacode
 */

/**
 * Datacode en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('datacode', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Datacode
 * @package   Datacode
 * @since     1.0.1
 */
return [
    'Datacode plugin loaded' => 'Datacode plugin loaded',
];
