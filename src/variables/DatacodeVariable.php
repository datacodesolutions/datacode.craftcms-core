<?php
/**
 * Datacode plugin for Craft CMS 3.x
 *
 * Datacode Solutions Plugin
 *
 * @link      datacodesolutions.com
 * @copyright Copyright (c) 2017 Datacode
 */
namespace datacode\core\variables;

use Craft;
use datacode\core\Plugin;

/**
 * Datacode Variable
 *
 * Craft allows plugins to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.datacode }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Datacode
 * @package   Datacode
 * @since     1.0.1
 */
class DatacodeVariable
{

}
