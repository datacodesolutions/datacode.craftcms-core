<?php

namespace datacode\core\services;

use craft\elements\db\TagQuery;
use craft\elements\GlobalSet;

class MetaService
{
    protected $entry = null;
    protected $global;
    protected $fallBack = [
        'metaTitle' => '',
        'metaKeywords' => '',
        'metaDescription' => '',
    ];

    public function __construct()
    {
        if (!isset($this->global)) {
            $this->global = $this->getFallback();
        }

        if ($this->global) {
            $this->fallBack = [
                'metaTitle' => $global->metaTitle,
                'metaKeywords' => implode(',', $global->metaKeywords->all()),
                'metaDescription' => $global->metaDescription
            ];
        }
    }

    public function description($context) : string
    {
        if ($description = $context['metaDescription'] ?? null) {
            return $description;
        }

        $page = $this->getEntry($context) ?? null;

        return $page['metaDescription'] ?? '';
    }

    public function keywords($context) : string
    {
        if ($keywords = $context['metaKeywords'] ?? null) {
            return $keywords;
        }

        $entry = $this->getEntry($context);

        if ($entry) {
            $metaKeywordsFieldValue = $entry->metaKeywords ?? null;

            if ($metaKeywordsFieldValue instanceof TagQuery) {
                $keywordTitles = [];

                foreach ($metaKeywordsFieldValue->all() as $keyword) {
                    $keywordTitles[] = $keyword->title;
                }

                return implode(',', $keywordTitles);
            }

            return (string) $metaKeywordsFieldValue;
        }

        return '';
    }

    public function ogImageUrl($context) : string
    {
        if ($ogImageUrl = $context['metaOgImageUrl'] ?? null) {
            return $ogImageUrl;
        }

        if ($elementCriteriaModel = $this->getEntry($context, 'metaOpenGraphImage')) {
            if ($assetFileModel = $elementCriteriaModel->one()) {
                return $assetFileModel->getUrl();
            }
        }

        return '';
    }

    public function ogTitle($context) : string
    {
        if ($ogUrl = $context['metaOgTitle'] ?? null) {
            return $ogUrl;
        }

        return '';
    }

    public function ogType($context) : string
    {
        if ($ogUrl = $context['metaOgType'] ?? null) {
            return $ogUrl;
        }

        return '';
    }

    public function ogUrl($context) : string
    {
        if ($ogUrl = $context['metaOgUrl'] ?? null) {
            return $ogUrl;
        }

        return '';
    }

    public function setEntry($entry)
    {
        $this->entry = $entry;
    }

    public function title($context) : string
    {
        if ($title = $context['metaTitle'] ?? null) {
            return $title;
        }

        $entry = $this->getEntry($context);

        if (!empty($entry['metaTitle'])) {
            return $entry['metaTitle'];
        } elseif ($entry) {
            return $entry['title'] . ' | ' . $context['siteName'];
        }

        return $context['siteName'];
    }

    protected function getEntry($context, $attribute = null)
    {
        $entry = $this->entry ?? $context['entry'] ?? null;

        if ($entry) {
            if ($attribute) {
                if (isset($entry->{$attribute})) {
                    return $entry->{$attribute};
                }
            } else {
                return $entry;
            }
        }

        $category = $context['category'] ?? null;

        if ($category) {
            if ($attribute) {
                if (isset($category->{$attribute})) {
                    return $category->{$attribute};
                }
            } else {
                return $category;
            }
        }

        return null;
    }

    /**
     * Get global
     *
     * @return mixed
     */
    protected function getFallback()
    {
        $global = GlobalSet::find()
            ->handle('seo')
            ->one();

        if (!$global) {
            return null;
        }

        return $global;
    }
}