<?php
/**
 * Datacode plugin for Craft CMS 3.x
 *
 * Datacode Solutions Plugin
 *
 * @link      datacodesolutions.com
 * @copyright Copyright (c) 2017 Datacode
 */
namespace datacode\core\services;

use Craft;
use craft\base\Component;
use datacode\core\Plugin;

/**
 * Datacode Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Datacode
 * @package   Datacode
 * @since     1.0.1
 */
class DatacodeService extends Component
{

}
