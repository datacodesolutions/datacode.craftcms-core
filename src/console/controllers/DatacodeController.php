<?php
/**
 * Datacode plugin for Craft CMS 3.x
 *
 * Datacode Solutions Plugin
 *
 * @link      datacodesolutions.com
 * @copyright Copyright (c) 2017 Datacode
 */

namespace datacode\core\console\controllers;

use Craft;
use yii\console\Controller;
use yii\helpers\Console;
use datacode\core\Plugin;

/**
 * Datacode Command
 *
 * The first line of this class docblock is displayed as the description
 * of the Console Command in ./craft help
 *
 * Craft can be invoked via commandline console by using the `./craft` command
 * from the project root.
 *
 * Console Commands are just controllers that are invoked to handle console
 * actions. The segment routing is plugin/controller-name/action-name
 *
 * The actionIndex() method is what is executed if no sub-commands are supplied, e.g.:
 *
 * ./craft datacode/datacode
 *
 * Actions must be in 'kebab-case' so actionDoSomething() maps to 'do-something',
 * and would be invoked via:
 *
 * ./craft datacode/datacode/do-something
 *
 * @author    Datacode
 * @package   Datacode
 * @since     1.0.1
 */
class DatacodeController extends Controller
{
    /**
     * Handle datacode/default console commands
     *
     * @return mixed
     */
    public function actionSyncdb($environment = 'production')
    {
        $logger = new class($this) {
            protected $console;
            protected $levels = [
                'info' => CONSOLE::FG_GREEN,
                'error' => CONSOLE::FG_RED,
                'normal' => CONSOLE::FG_YELLOW,
            ];

            public function __construct($console)
            {
                $this->console = $console;
            }

            public function log($text, $level = 'normal')
            {
                $this->console->stdout($text, $this->levels[$level]);
                $this->console->stdout(PHP_EOL);
            }
        };

        datacode('syncdb')->sync($logger, $environment);
    }

    public function actionDumpmysql()
    {
        datacode('syncdb')->dumpMysql();
    }
}
