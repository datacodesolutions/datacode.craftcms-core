<?php
/**
 * Datacode plugin for Craft CMS 3.x
 *
 * Datacode Solutions Plugin
 *
 * @link      datacodesolutions.com
 * @copyright Copyright (c) 2017 Datacode
 */

namespace datacode\core\behaviors\users;

use Craft;
use yii\base\Behavior;
use datacode\core\Plugin;

class BaseUserBehavior extends Behavior
{

}