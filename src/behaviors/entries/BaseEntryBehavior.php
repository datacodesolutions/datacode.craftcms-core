<?php
/**
 * Datacode plugin for Craft CMS 3.x
 *
 * Datacode Solutions Plugin
 *
 * @link      datacodesolutions.com
 * @copyright Copyright (c) 2017 Datacode
 */

namespace datacode\core\behaviors\entries;

use Craft;
use yii\base\Behavior;
use datacode\core\Plugin;
use datacode\core\behaviors\traits\AssetTrait;

class BaseEntryBehavior extends Behavior
{
    use AssetTrait;

    public function _one($field, $relatedField)
    {
        $model = clone($this->owner);

        try {
            $relatedElement = $model->{$field}[0] ?? null;

            if($relatedElement) {
                return $relatedElement->{$relatedField};
            }
        } catch(Exception $e) {}

        return null;
    }
}