<?php
/**
 * Datacode plugin for Craft CMS 3.x
 *
 * Datacode Solutions Plugin
 *
 * @link      datacodesolutions.com
 * @copyright Copyright (c) 2017 Datacode
 */
namespace datacode\core;

use Craft;
use craft\base\Plugin as BasePlugin;
use craft\console\Application as ConsoleApplication;
use craft\events\PluginEvent;
use craft\services\Plugins;
use craft\web\twig\variables\CraftVariable;
use datacode\core\services\BehaviorService;
use datacode\core\services\SyncDbService;
use datacode\core\services\DatacodeService;
use datacode\core\services\MetaService;
use datacode\core\twigextensions\DatacodeTwigExtension;
use datacode\core\variables\DatacodeVariable;
use yii\base\Event;

/**
 * Craft plugins are very much like little applications in and of themselves. We’ve made
 * it as simple as we can, but the training wheels are off. A little prior knowledge is
 * going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL,
 * as well as some semi-advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 *
 * @author    Datacode
 * @package   Datacode
 * @since     1.0.1
 *
 * @property  DatacodeService $datacode
 */
class Plugin extends BasePlugin
{
    // Static Properties
    // =========================================================================

    /**
     * Static property that is an instance of this plugin class so that it can be accessed via
     * Datacode::$plugin
     *
     * @var Datacode
     */
    public static $plugin;

    // Public Methods
    // =========================================================================

    /**
     * Set our $plugin static property to this class so that it can be accessed via
     * Datacode::$plugin
     *
     * Called after the plugin class is instantiated; do any one-time initialization
     * here such as hooks and events.
     *
     * If you have a '/vendor/autoload.php' file, it will be loaded for you automatically;
     * you do not need to load it in your init() method.
     *
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        // Set initial base components first
        $this->setComponents([
            'datacode' => DatacodeService::class,
            'syncdb' => SyncDbService::class,
            'behavior' => BehaviorService::class,
            'meta' => MetaService::class
        ]);

        // Add in our console commands
        if (Craft::$app instanceof ConsoleApplication) {
            $this->controllerNamespace = 'datacode\core\console\controllers';
        }

        /**
         * Settings
         */
        $alias = $this->getSettings()->alias;
        $namespace = $this->getSettings()->namespace;
        $path = $this->getSettings()->folderPath;

        if (isset($alias) && isset($namespace) && isset($path)) {
           Craft::setAlias($alias, $path);

           $services = $this->getSettings()->services;
           if (isset($services)) {
               foreach ($services as $name => $service) {
                   $this->setComponents([
                       $name => $service,
                   ]);
               }
           }

           if (Craft::$app instanceof \craft\web\Application) {
               $controllerNamespace = $this->getSettings()->controllerNamespace;
               if (isset($controllerNamespace)) {
                   $this->controllerNamespace = $controllerNamespace;
               }
           }

           if (Craft::$app instanceof ConsoleApplication) {
               $consoleControllerNamespace = $this->getSettings()->consoleControllerNamespace;
               if (isset($consoleControllerNamespace)) {
                   $this->controllerNamespace = $consoleControllerNamespace;
               }
           }

           $twigextension = $this->getSettings()->twigextension;
           if ($twigextension) {
               Craft::$app->view->twig->addExtension(new $twigextension);
           }

           $extender = $this->getSettings()->extender;
           if ($extender && method_exists($extender, 'register')) {
               $extender::register();
           }
        }

        // Add base twig extensions
        Craft::$app->view->twig->addExtension(new DatacodeTwigExtension());

        // listen to init
        $this->behavior->listen();

        // Register our variables
        $variables = $this->getSettings()->variables;
        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) use($variables) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('datacode', DatacodeVariable::class);

                if (isset($variables)) {
                    foreach ($variables as $n => $v) {
                        $variable->set($n, $v);
                    }
                }
            }
        );

        // Do something after we're installed
        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                    if (!file_exists(CRAFT_BASE_PATH.DIRECTORY_SEPARATOR.'config/datacode.php')) {
                        copy(__DIR__.DIRECTORY_SEPARATOR.'config-template.php', CRAFT_BASE_PATH.DIRECTORY_SEPARATOR.'config/datacode.php');
                    }
                }
            }
        );

        /**
         * Logging in Craft involves using one of the following methods:
         *
         * Craft::trace(): record a message to trace how a piece of code runs. This is mainly for development use.
         * Craft::info(): record a message that conveys some useful information.
         * Craft::warning(): record a warning message that indicates something unexpected has happened.
         * Craft::error(): record a fatal error that should be investigated as soon as possible.
         *
         * Unless `devMode` is on, only Craft::warning() & Craft::error() will log to `craft/storage/logs/web.log`
         *
         * It's recommended that you pass in the magic constant `__METHOD__` as the second parameter, which sets
         * the category to the method (prefixed with the fully qualified class name) where the constant appears.
         *
         * To enable the Yii debug toolbar, go to your user account in the AdminCP and check the
         * [] Show the debug toolbar on the front end & [] Show the debug toolbar on the Control Panel
         *
         * http://www.yiiframework.com/doc-2.0/guide-runtime-logging.html
         */
        Craft::info(
            Craft::t(
                'datacode',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================
    protected function createSettingsModel()
    {
        return new Settings();
    }
}