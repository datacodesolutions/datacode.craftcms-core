<?php

return [
    /**
     * Namespace for the plugin extenders
     */
    "alias" => "@yournamespace",

    /**
     * Namespace for the plugin extenders
     */
    "namespace" => "yournamespace\\",
    
    /**
     * Path to extenders we will autoload via ->addPsr4()
     */
    "folderPath" => CRAFT_BASE_PATH . '/yournamespace',
    
    /**
     * Namespace for the plugin extenders
     */
    "extender" => null,

    /**
     * Namespace for the plugin extenders
     */
    "controllerNamespace" => null,

    /**
     * Namespace for the plugin extenders
     */
    "consoleControllerNamespace" => null,

    /**
     * Namespace for the plugin extenders
     */
    "services" => [],

    /**
     * Namespace for the plugin extenders
     */
    "twigextension" => null,

    /**
     * Namespace for the plugin extenders
     */
    "variables" => [],

    /**
     * Namespace for the plugin extenders
     */
    "remotes" => []
];
